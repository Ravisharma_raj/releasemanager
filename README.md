Release manager is a spring boot application.

Technologies used:

1. Spring boot
2. Java 11
3. Maven



Key measures taken while development:

1. Code readibility.
2. Code maintaintability.
3. Seperation of concerns between layers.
4. Code testability


It has two apis exposed currently  

Api 1: GET localhost:8081/releasemanager/services?systemVersion=2  

Sample response: 

[
{
"version": 1,
"name": "se-1"
},
{
"version": 1,
"name": "se-2"
},
{
"version": 1,
"name": "se-2"
}
]


Api 2 : POST localhost:8081/releasemanager/deploy  

Request body sample :{"name":"se-1","version":1}  

Sample response : 1



To run the application , just import the project as maven project in any ide (intelij , spring sts) , and run the file ReleaseMangerApplication.java or create a pacakge (jar) by running mvn pakage and then run the jar file. the default port configured is 8081.
