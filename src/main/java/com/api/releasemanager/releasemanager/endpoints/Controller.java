package com.api.releasemanager.releasemanager.endpoints;

import com.api.releasemanager.releasemanager.dto.DeployDto;
import com.api.releasemanager.releasemanager.services.DeployService;
import com.api.releasemanager.releasemanager.services.UtilityService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/releasemanager")
public class Controller {

    DeployService deployService;
    UtilityService utilityService;

    public Controller(DeployService deployService, UtilityService utilityService) {
        this.deployService = deployService;
        this.utilityService = utilityService;
    }


    @GetMapping
    public String healthCheck() {

        return "alive";
    }

    @GetMapping("/services")
    public ResponseEntity systemVersion(@RequestParam("systemVersion") Integer systemVersion) {

        if (!utilityService.isSystemVersionValid(systemVersion)) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        List<DeployDto> result = deployService.getServiceData().get(systemVersion).getDeployDtoList();
        return new ResponseEntity(result, HttpStatus.ACCEPTED);


    }

    @PostMapping("/deploy")
    public ResponseEntity deploy(@RequestBody DeployDto deployDto) {
        return new ResponseEntity(deployService.updateServiceData(deployDto), HttpStatus.CREATED);


    }

}
