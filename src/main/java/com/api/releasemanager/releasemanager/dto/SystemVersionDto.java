package com.api.releasemanager.releasemanager.dto;

import lombok.Data;

import java.util.List;

@Data
public class SystemVersionDto {

    List<DeployDto> deployDtoList;
}
