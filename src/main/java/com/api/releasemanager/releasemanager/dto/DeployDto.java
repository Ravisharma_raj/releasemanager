package com.api.releasemanager.releasemanager.dto;

import lombok.Data;

@Data
public class DeployDto {

    int version;
    String name;
}
