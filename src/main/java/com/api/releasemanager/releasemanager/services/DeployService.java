package com.api.releasemanager.releasemanager.services;

import com.api.releasemanager.releasemanager.dto.DeployDto;
import com.api.releasemanager.releasemanager.dto.SystemVersionDto;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class DeployService {

    static int activeSystemVersion = 0;

    Map<Integer, SystemVersionDto> servicesData = new LinkedHashMap<>();

    public Map<Integer, SystemVersionDto> getServiceData() {

        return servicesData;
    }

    public int updateServiceData(DeployDto deployDto) {

        if (servicesData.isEmpty()) {
            return deployFirstService(deployDto);
        }

        if (isThisNewService(deployDto.getName())) {

            return createNewSystemVersion(deployDto);
        }
        int result = checkNewSystemVersionNeeded(deployDto);

        if (result == 0) {
            return createNewSystemVersion(deployDto);

        }

        return result;
    }

    private int deployFirstService(DeployDto deployDto) {
        SystemVersionDto systemVersionDto = new SystemVersionDto();
        List<DeployDto> deployDtoList = new ArrayList<>();
        deployDtoList.add(deployDto);
        systemVersionDto.setDeployDtoList(deployDtoList);
        servicesData.put(1, systemVersionDto);
        return activeSystemVersion = 1;
    }

    private int createNewSystemVersion(DeployDto deployDto) {
        List<DeployDto> deployDtoList = servicesData.get(activeSystemVersion).getDeployDtoList();
        List<DeployDto> deployDtoList1 = new ArrayList<>();
        deployDtoList1.addAll(deployDtoList);
        upgradeTheExistingServiceVersion(deployDtoList1 , deployDto);
        int size = servicesData.keySet().size();
        SystemVersionDto systemVersionDto = new SystemVersionDto();
        systemVersionDto.setDeployDtoList(deployDtoList1);
        servicesData.put(size + 1, systemVersionDto);
        activeSystemVersion = size + 1;
        return activeSystemVersion;

    }

    private void upgradeTheExistingServiceVersion(List<DeployDto> deployDtoList1, DeployDto deployDto) {

        for(DeployDto dto:deployDtoList1){

            if (dto.getName().equalsIgnoreCase(deployDto.getName())){

                deployDtoList1.remove(dto);
                break;
            }
        }
        deployDtoList1.add(deployDto);
    }

    private Integer checkNewSystemVersionNeeded(DeployDto deployDto) {

        SystemVersionDto systemVersionDto = servicesData.get(activeSystemVersion);
        List<DeployDto> list = systemVersionDto.getDeployDtoList();
        int currentlyDeployedServicesCount = list.size();

        for (Integer key : servicesData.keySet()) {

            if (servicesData.get(key).getDeployDtoList().size() == currentlyDeployedServicesCount) {
                int result = checkExistingSystemVersions(key, deployDto);

                if (result != 0) {
                    return result;
                }
            }

        }
        return 0;
    }

    private boolean isThisNewService(String name) {
        List<DeployDto> deployDtoList = servicesData.get(activeSystemVersion).getDeployDtoList();
        for (DeployDto dto : deployDtoList) {
            if (dto.getName().equalsIgnoreCase(name)) {
                return false;
            }
        }
        return true;
    }

    private int checkExistingSystemVersions(Integer existingVersion, DeployDto deployDto) {
        SystemVersionDto systemVersionDto = servicesData.get(existingVersion);
        int result = 0;
        if (systemVersionDto.getDeployDtoList().contains(deployDto)) {

            if (systemVersionDto.getDeployDtoList().size() == 1) { //case where only one service is deployed in the system

                return existingVersion;
            }

            List<DeployDto> tempDto = new ArrayList<>(); //here is the start of logic to check if any existing system version already exist after the requested deployement
            tempDto.addAll(systemVersionDto.getDeployDtoList());
            tempDto.remove(deployDto);

            List<DeployDto> activeDeployDtoList = servicesData.get(activeSystemVersion).getDeployDtoList();
            List<DeployDto> tempDtoForActiveService = new ArrayList<>();
            tempDtoForActiveService.addAll(activeDeployDtoList);
            for (DeployDto dto : tempDtoForActiveService) {
                if (dto.getName().equalsIgnoreCase(deployDto.getName())){
                    tempDtoForActiveService.remove(dto);
                    break;
                }
            }
            result = compareServiceList(tempDto, tempDtoForActiveService);
        }
        if (result == 1) {
            activeSystemVersion = existingVersion;
            return existingVersion;
        }
        return result;
    }

    private int compareServiceList(List<DeployDto> tempDto, List<DeployDto> activeDeployDtoList) {

        if (tempDto.equals(activeDeployDtoList)) {
            return 1;
        }
        return 0;
    }


}
