package com.api.releasemanager.releasemanager.services;

import org.springframework.stereotype.Service;

@Service
public class UtilityService {

    DeployService deployService;

    public UtilityService(DeployService deployService) {
        this.deployService = deployService;
    }

   public boolean isSystemVersionValid(Integer systemVersion){

        if(systemVersion == null){
            systemVersion = DeployService.activeSystemVersion;
        }
        int maximumKeyValue=deployService.getServiceData().keySet().size();
        if (maximumKeyValue<systemVersion){
            return false;
        }

        return true;
   }
}
